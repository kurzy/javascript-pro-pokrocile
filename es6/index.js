/* eslint-disable no-shadow */
/* eslint-disable func-names */

/* eslint-disable block-scoped-var */
/* eslint-disable no-var */
/* eslint-disable vars-on-top */
/* eslint-disable no-plusplus */
/* eslint-disable no-unused-vars */

/* eslint-disable one-var-declaration-per-line */
/* eslint-disable one-var */

/* eslint-disable max-classes-per-file */

// let
{
  const a = 5;
  var b = 42;
  const c = Infinity;

  console.log(a, b, c);
}

console.log(b);

// viz příklady ze začátku
/*
const buttons = function () {
  const container = document.getElementById("buttons");

  for (let b = 1; b <= 5; b++) {
    const button = document.createElement("button");

    button.textContent = b;

    button.addEventListener("click", function () {
      console.log(b);
    });

    container.appendChild(button);
  }
};

buttons();
*/
// arrow funkce
function quad01(x) {
  return x ** 2;
}

const quad02 = function (x) {
  return x ** 2;
};

const quad03 = (x) => x ** 2;

const quad04 = (x) => x ** 2;

const quad05 = (x) => x ** 2;

const fixTo2 = (x) => x
  .toFixed(2)
  * 2;

console.log(quad05(8));
console.log(fixTo2(8.5585455));
/*
const Form1 = function (value) {
  this._dom = {
    button: document.createElement('button'),
    input: document.createElement('input'),
  };

  this._dom.input.type = 'text';
  this._dom.input.value = value;

  this._dom.button.textContent = 'klikni';

  document.body.appendChild(this._dom.input);
  document.body.appendChild(this._dom.button);

  this._dom.button.addEventListener(
    'click',
    () => {
      console.log(this._dom.input.value);
    },
  );
};

new Form1('ahoj')
*/
// parametry funkcí

const changeSalary1 = (salary, amount) => {
  const newAmount = amount ?? 2;

  const newSalary = salary + newAmount;

  return newSalary;
};

const changeSalary2 = (salary, amount = 0) => salary + amount;

console.log(changeSalary2(1000, 500));
console.log(changeSalary2(1000));

const sum = (a, b, ...rest) => {
  const asumb = a + b;

  const summary = rest.reduce(
    /* (acc, x) => {
      const result = acc + x;

      return result;
    }, */
    (acc, x) => acc + x,
    asumb,
  );

  return summary;
};

console.log(sum(1, 2));
console.log(sum(1, 2, 3));
console.log(sum(1, 2, 3, 4));
console.log(sum(1, 2, 3, 4, 5));

// Template Literals
let jmeno = 'Vilém Lipold';
const povolani = 'programátor';
const cislo1 = 5, cislo2 = 6;

// rychlé vytváření objektů
const vek = 40, pozdrav = () => 'Milášek!';
jmeno = 'Bilbo pytlík';

const hobbit = {
  jmeno,
  pozdrav,
  vek,
};

console.log(hobbit);

const numsToSum = [1, 3, 5, 7, 11, 13];
console.log(sum(...numsToSum));

const numsToSumCopy = [...numsToSum];
numsToSum.push(17);

console.log(numsToSum, numsToSumCopy);

const frodo = { ...hobbit, jmeno: 'Frodo Pytlík' };

console.log(hobbit, frodo);

const [one, , three] = numsToSum;

console.log(one, three);

const { jmeno: hobbitName } = hobbit;

console.log(hobbitName);

const hobbits = {
  hobbit,
  frodo,
};

console.log(hobbits);

const { frodo: { jmeno: frodoName } } = hobbits;
console.log(frodoName);

// nové příkazy
// Object.assign
const def = {
  a: 1,
  b: 2,
  c: 3,
  d: 4,
};
const cil = {
  a: 11,
  c: 13,
};

// Array.prototype.find a Array.prototype.findIndex
const array = [1, 2, 3, 4, 5, 6, 7, 8];

const exist = array.includes(15);
console.log(exist);

const anyNumber = array.find((x) => x > 5);
console.log(anyNumber);

const url = '/pages/contact';

const fixedUrl = url.startsWith('/') ? url : `/${url}`;
console.log(fixedUrl);

// String.prototype.repeat

const word = 'money, ';
console.log(word.repeat(3));

// vyhledávání v řetezcích
const hora = 'nangaparbat';

// matematické a číselné věci
console.log(2 ** 53 === 2 ** 53 + 1);
console.log(Number.isSafeInteger(2 ** 53));

// Number.EPSILON

console.log(0.1 + 0.2 === 0.3);
console.log(0.2 + 0.2 - 0.4 < Number.EPSILON);
console.log(2 + 2 - 4 < Number.EPSILON);

console.log(Math.sign(-5));
console.log(Math.sign(5));

console.log(Math.sign(-0));
console.log(Math.sign(0));
console.log(Math.sign(0) === Math.sign(-0));

const fibonacci = (n) => {
  const base = [0n, 1n, 1n];

  if (n < 4) {
    return base;
  }

  const seq = Array(n - 3).fill(1).reduce(
    (acc) => {
      const x1 = acc.at(-1);
      const x2 = acc.at(-2);

      acc.push(x1 + x2);

      return acc;
    },
    base,
  );

  return seq;
};

const fib = fibonacci(100);
const evenCount = fib.filter((x) => !(x % 2n)).length;

console.log(fib, evenCount);

// formátování čísel, data a měny
const cz = 'cs';
const us = 'en-US';
const gb = 'en-GB';
const de = 'de-DE';

console.log(navigator.language, navigator.languages);

const csnf = new Intl.NumberFormat(cz, { style: 'decimal', roundingPriority: 'morePrecision' });
const usnf = new Intl.NumberFormat(us);
const gbnf = new Intl.NumberFormat(gb);
const denf = new Intl.NumberFormat(de);
const janf = new Intl.NumberFormat('ja');

console.log(csnf.format(12_345_678.9876));
console.log(usnf.format(12_345_678.9876));
console.log(gbnf.format(12_345_678.9876));
console.log(denf.format(12_345_678.9876));
console.log(janf.format(12_345_678.9876));

const cscf = new Intl.NumberFormat(cz, { style: 'currency', currency: 'CZK' });
const uscf = new Intl.NumberFormat(us, { style: 'currency', currency: 'USD' });
const gbcf = new Intl.NumberFormat(gb, { style: 'currency', currency: 'GBP' });
const decf = new Intl.NumberFormat(de, { style: 'currency', currency: 'EUR' });
const arcf = new Intl.NumberFormat('ar', { style: 'currency', currency: 'LBP' });
const jacf = new Intl.NumberFormat('ja', { style: 'currency', currency: 'JPY' });

console.log(cscf.format(12_345_678.9876));
console.log(uscf.format(12_345_678.9876));
console.log(gbcf.format(12_345_678.9876));
console.log(decf.format(12_345_678.9876));
console.log(arcf.format(12_345_678.9876));
console.log(jacf.format(12_345_678.9876));

// prvně zkusit bez options, pak s nimi
const options = {
  weekday: 'long',
  year: 'numeric',
  month: 'long',
  day: 'numeric',
  hour: 'numeric',
  minute: 'numeric',
};

const csdf = new Intl.DateTimeFormat(cz, options);
const gbdf = new Intl.DateTimeFormat(gb, options);
const usdf = new Intl.DateTimeFormat(us, options);
const dedf = new Intl.DateTimeFormat(de, options);
const ardf = new Intl.DateTimeFormat('ar', options);
const jadf = new Intl.DateTimeFormat('ja', options);

console.log(csdf.format(new Date()));
console.log(gbdf.format(new Date()));
console.log(usdf.format(new Date()));
console.log(dedf.format(new Date()));
console.log(ardf.format(new Date()));
console.log(jadf.format(new Date()));

// řazení položek v poli (ES5)
const dny = ['pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota', 'neděle'];
const sortedDays = dny.toSorted((prev, next) => prev.localeCompare(next));

console.log(dny);
console.log(sortedDays);

// Promisy
const randPromise = new Promise(
  (resolve, reject) => {
    if (Math.random() > 0.5) {
      reject(new Error('Spadlo ti to, Máchale!'));

      return;
    }

    resolve('Stojí to');
  },
);

// eslint-disable-next-line no-console
console.info('promise běží');

randPromise
  .then((data) => console.log(data))
  .catch((data) => console.warn(data));

let round = 0;
const interval = setInterval(
  () => {
    if (round > 40) {
      clearInterval(interval);
    }

    console.log(round);
    round++;
  },
  250,
);

const hellPromise = new Promise(
  (jeToCajk) => {
    setTimeout(
      () => jeToCajk('Získals hodnost vraníka'),
      5000,
    );
  },
);

hellPromise
  .then(console.log);

const timeout = (multiplicator) => new Promise(
  (resolve) => {
    if (typeof multiplicator !== 'number') {
      throw new Error('Bad multiplicator type - must be number!');
    }

    setTimeout(
      () => resolve(multiplicator),
      multiplicator * 1000,
    );
  },
);

const chainStart = new Date();

timeout(1)
  .then((data) => {
    console.log(`Promise ${data} skončila v čase ${Date.now() - chainStart} ms.`);

    return timeout(2);
  })
  .then((data) => {
    console.log(`Promise ${data} skončila v čase ${Date.now() - chainStart} ms.`);

    return timeout(3);
  })
  .then((data) => {
    console.log(`Promise ${data} skončila v čase ${Date.now() - chainStart} ms.`);

    return timeout('pivo');
  })
  .then((data) => {
    console.log(`Promise ${data} skončila v čase ${Date.now() - chainStart} ms.`);

    return timeout(5);
  })
  .then((data) => {
    console.log(`Promise ${data} skončila v čase ${Date.now() - chainStart} ms.`);
  })
  .catch(console.error);

const fetchPromise = fetch('http://ip-api.com/json/');

fetchPromise
  .then((response) => {
    const { ok, status } = response;

    if (!ok) {
      throw new Error(`Status code ${status}`);
    }

    response.json()
      .then((data) => {
        const { lat, lon } = data;

        console.log(`N${lat} E${lon}`);
      });
  })
  .catch(console.error);

// asynchronní funkce async/await

const chainFn = async () => {
  try {
    let answer = await timeout(1);
    console.log(`Skončila promise v async ${answer}.`);

    answer = await timeout(2);
    console.log(`Skončila promise v async ${answer}.`);

    answer = await timeout(3);
    console.log(`Skončila promise v async ${answer}.`);

    answer = await timeout(4);
    console.log(`Skončila promise v async ${answer}.`);
  } catch (error) {
    console.error(error);
  }
};

chainFn();

const getGeoIp = async (credentials) => {
  try {
    const response = await fetch('http://ip-api.com/json/', {
      headers: {
        'X-my-header': 'some',
      },
    });

    const {
      json, ok, status, headers,
    } = response;

    console.log(response.headers, credentials);

    if (!ok) {
      throw new Error(`Status code ${status}`);
    }

    const { lat, lon } = await response.json();
    console.log(`https://mapy.cz/turisticka?q=${lat}%20${lon}&source=coor&id=${lon}%2C${lat}&x=${lon}&y=${lat}&z=14`);
  } catch (error) {
    console.error(error);
  }
};

console.log('konec promisí');

((c) => {
  const credentials = {
    token: '',
    password: '',
  };
  console.log('Tohle se spustí samo');
  console.log(c);
  const x = 42;

  getGeoIp(credentials);
})('param');

// Set

const someValues = [1, 2, 3, 5, 2, 3, 4, 5, 6, 8, 2, 3];
console.log(someValues);
const uniqueSet = new Set(someValues);
console.log(uniqueSet);

uniqueSet.add(42);
uniqueSet.add(3);
console.log(uniqueSet);

const uniqueArray = Array.from(uniqueSet);
console.log(uniqueArray);

// Třídy
class Pet {
  #color;

  #name;

  constructor(color, name) {
    this.#color = color;
    this.#name = name;
  }

  whois() {
    return `My name is ${this.#name}, color: ${this.#color}`;
  }

  toString() {
    return `${this.#name}/${this.#color}`;
  }

  get name() {
    return this.#name;
  }

  get color() {
    return this.#color;
  }
}

const harry = new Pet('red', 'Harry');

console.log(harry.whois());
console.log(harry.name);
console.log(`harry: ${harry}`);

class Dog extends Pet {
  #age;

  constructor(age, color, name) {
    super(color, name);
    this.publicName = name;

    this.#age = age;
  }

  toString() {
    const petString = super.toString();

    return `${petString}/${this.#age}`;
  }

  set vek(value) {
    if (typeof value !== 'number') {
      throw new Error('Bad age value');
    }

    if (value > 50 || value < 1) {
      throw new RangeError('your dog is dead');
    }

    this.#age = value;
  }
}

const myDog = new Dog(42, 'brown', 'Žeryk');

console.log(myDog.whois());
console.log(myDog.name);
console.log(myDog.publicName);
myDog.vek = 5;
console.log(`${myDog}`);
