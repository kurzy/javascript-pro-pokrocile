import { createServer } from 'node:http';
import { existsSync, lstatSync, readFile } from 'node:fs';
import { parse } from 'node:path';
import setCors from './modules/setCors.mjs';

const hostName = '127.0.0.1';
const port = 3000;
const serverName = `http://${hostName}:${port}/`;

const server = createServer((req, res) => {
  // parsujeme URL
  const { url } = req;
  const urlObject = new URL(url, serverName);

  // extrahujeme URL cestu
  const { pathname } = urlObject;
  let pathName = `.${pathname}`;

  // na základě cesty určíme koncovku souboru .js, .doc, ...
  // mapa koncovek a jim odpovídající MIME typ
  const map = {
    '.ico': 'image/x-icon',
    '.html': 'text/html',
    '.js': 'text/javascript',
    '.mjs': 'text/javascript',
    '.json': 'application/json',
    '.css': 'text/css',
    '.png': 'image/png',
    '.jpg': 'image/jpeg',
    '.wav': 'audio/wav',
    '.mp3': 'audio/mpeg',
    '.svg': 'image/svg+xml',
    '.pdf': 'application/pdf',
    '.doc': 'application/msword',
  };

  let { ext } = parse(pathName);

  // jde o adresář?
  if (existsSync(pathName) && lstatSync(pathName).isDirectory()) {
    if (!pathName.endsWith('/')) {
      console.log(`Redirect to folder ${pathName}/`);

      res.writeHead(
        302,
        {
          Location: `${pathName}/`,
        },
      );

      res.end();

      return;
    }

    pathName = `${pathName}index.html`;
    ext = '.html';
  }

  // požadavek do konzole
  console.log(`${req.method} ${req.url} ${pathName} ${ext}`);

  if (!existsSync(pathName)) {
    // pokud soubor (adresář) neexistuje, vracíme 404
    res.statusCode = 404;
    res.setHeader('Content-Type', 'text/plain; charset=utf-8');

    res.end('Ajajaj, asi tady není co hledáš.');
  }

  // přečteme soubor
  readFile(pathName, (err, data) => {
    if (err) { // nastala chyba čtení
      res.statusCode = 500;
      res.setHeader('Content-Type', 'text/plain; charset=utf-8');
      res.end(`Error getting the file: ${err}.`);
      return;
    }
    // soubor nalezen, nastavíme mime typ
    res.setHeader('Content-Type', map[ext] || 'text/plain');
    // pokud je třeba na soubory přistupovat z jiného serveru, musí se pořešit CORS
    setCors(res);

    res.end(data);
  });
});

server.listen(port, hostName, () => {
  console.log(`Server běží na ${serverName}`);
});
